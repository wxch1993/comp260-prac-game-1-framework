using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	public float maxSpeed = 5.0f;
	public float acceleration = 3.0f;
	public float brake = 5.0f;
	public float turnSpeed = 90.0f;
	public float destroyRadius = 1.0f;
	private float newTurnSpeed = 90.0f;
	private float speed = 0.0f;
	private BeeSpawner beeSpawner;

	void Start () {
	    beeSpawner = FindObjectOfType<BeeSpawner> ();
	}

	void Update () {
	    if(speed > 1) newTurnSpeed = turnSpeed/(Mathf.Sqrt(speed));
		float forwards = Input.GetAxis("Vertical");
		float turn = Input.GetAxis("Horizontal");
		if(speed > 0) transform.Rotate(0, 0, -turn * newTurnSpeed * Time.deltaTime);
		if(speed < 0) transform.Rotate(0, 0, +turn * newTurnSpeed * Time.deltaTime);
		if(forwards > 0) {
		    speed = speed + acceleration * Time.deltaTime;
		}
		else if(forwards < 0) {
		    speed = speed - acceleration * Time.deltaTime;
		}

		else {
		    if(forwards == 0){
		    if(speed > 1) {
            		        speed = speed - brake * Time.deltaTime;
            		        if(speed < 0) speed = 0;
            		    }
            if(speed < -1) {
            		        speed = speed + brake * Time.deltaTime;
            		        if(speed > 0) speed = 0;
            		    }
		    }
		}

		if (Input.GetButtonDown ("Fire1")) {
		        beeSpawner.DestroyBees(transform.position, destroyRadius);
		    }

		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		Vector2 velocity = Vector2.up * speed;

		transform.Translate(velocity * Time.deltaTime);
	}
}
