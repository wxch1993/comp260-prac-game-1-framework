using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;
	private float speed;
	private float turnSpeed;
	private Transform target;
	private Vector2 heading = Vector2.right;
	public ParticleSystem explosionPrefab;
	public PlayerMove player;
	
	void Start() {
		PlayerMove player = FindObjectOfType<PlayerMove>();
		target = player.transform;
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
	}

	void Update() {
		Vector2 direction = target.position - transform.position;
		
		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;
		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
		// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}
		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDestroy() {
	    ParticleSystem explosion = Instantiate(explosionPrefab);
	    explosion.transform.position = transform.position;
	}
}
