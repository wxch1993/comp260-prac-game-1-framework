using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
	public int nBees = 50;	
	public BeeMove beePrefab;
	public Rect spawnRect;
	public int minSpawnRate = 50;
	public int maxSpawnRate = 10;
	private float frequency = 50;
	private int i;
	void OnDrawGizmos() {
		// draw the spawning rectangle
		Gizmos.color = Color.green;
		Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMin), 
                new Vector2(spawnRect.xMax, spawnRect.yMin));
		Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMin), 
                new Vector2(spawnRect.xMax, spawnRect.yMax));
		Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMax), 
                new Vector2(spawnRect.xMin, spawnRect.yMax));
		Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMax), 
                new Vector2(spawnRect.xMin, spawnRect.yMin));
	}

	void Start () {
		for (i = 0; i < nBees; i++) {
			BeeMove bee = Instantiate(beePrefab);
			bee.transform.parent = transform;
			bee.gameObject.name = "Bee " + i;
			bee.transform.position = new Vector2(spawnRect.xMin + Random.value * spawnRect.width, spawnRect.yMin + Random.value * spawnRect.height);
		}	
	}

	void Update () {
		if(frequency > 0) {
			frequency = frequency - 1;
		}
		else {
			BeeMove bee = Instantiate(beePrefab);
			bee.transform.parent = transform;
			bee.gameObject.name = "Bee " + i;
			bee.transform.position = new Vector2(spawnRect.xMin + Random.value * spawnRect.width, spawnRect.yMin + Random.value * spawnRect.height);
			i++;
			frequency = Mathf.Lerp(maxSpawnRate, minSpawnRate, Random.value);
			if(frequency < 1) frequency = 1;
		}
		
	}

	public void DestroyBees(Vector2 centre, float radius) {
            for (int i = 0; i < transform.childCount; i++) {
                Transform child = transform.GetChild(i);
                Vector2 v = (Vector2)child.position - centre;
                if (v.magnitude <= radius) {
                    Destroy(child.gameObject);
                }
            }
        }

}
